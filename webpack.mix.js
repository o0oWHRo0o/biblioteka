const mix = require('laravel-mix');

//stylesheets
mix.sass('resources/sass/app.scss', 'public/css')
    .options({
        processCssUrls: false
    })

//javascript
mix.js('resources/js/app.js', 'public/js')

//sourceMaps
if(mix.inProduction()){
    mix.version();
} else {
    mix.sourceMaps(false, 'inline-source-map');
    mix.browserSync('http://biblioteka.test/');
}

