<?php

use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('/', 'App\Http\Controllers\BookController@index')->name('index');
Route::get('/add', 'App\Http\Controllers\BookController@add')->name('add');
Route::post('/store', 'App\Http\Controllers\BookController@store')->name('store');
Route::get('/{book}', 'App\Http\Controllers\BookController@show')->name('show');
Route::get('/{book}/edit', 'App\Http\Controllers\BookController@edit')->name('edit');
Route::put('/{book}', 'App\Http\Controllers\BookController@update')->name('update');
Route::delete('/{book}', 'App\Http\Controllers\BookController@delete')->name('delete');

Route::get('/api/get_books', 'App\Http\Controllers\BookController@get_books')->name('get_books');
