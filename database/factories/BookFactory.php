<?php

namespace Database\Factories;

use App\Models\Book;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(
                $this->faker->numberBetween(1, 5)
            ),
            'author' => $this->faker->name(),
            'release_date' => $this->faker->date(),
        ];
    }
}
