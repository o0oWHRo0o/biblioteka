<?php

namespace App\Jobs;

use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Str;
use App\Models\Book;

class CreateUniqueBookSlug
{
    use Dispatchable, SerializesModels;

    protected $book;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Book $book)
    {
        $this->book = $book;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $slug = $this->getCurrentBookSlug();
        $similarSlugs = $this->getSimilarSlugs($slug);

        if ($similarSlugs->isNotEmpty()) {
            $similarCount = $similarSlugs->count();
            $slug = Str::slug("{$slug}-{$similarCount}");
        }
        $this->book->slug = $slug;
    }

    protected function getCurrentBookSlug()
    {
        return Str::slug($this->book->title);
    }

    protected function getSimilarSlugs(string $slug)
    {
        return Book::select('slug')
            ->where('slug', 'LIKE', "$slug%")
            ->where('id', '<>', $this->book->id)
            ->get();
    }
}
