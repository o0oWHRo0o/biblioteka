<?php

namespace App\Observers;

use App\Jobs\CreateUniqueBookSlug;
use App\Models\Book;

class BookObserver
{
    public function creating(Book $book)
    {
        CreateUniqueBookSlug::dispatch($book);
    }

    public function updating(Book $book)
    {
        CreateUniqueBookSlug::dispatch($book);
    }
}
