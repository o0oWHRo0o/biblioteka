<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Http\Requests\BookStoreRequest;
use App\Http\Requests\UpdateBookStoreRequest;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Show all books
     *
     * @return void
     */
    public function index()
    {
        return view('books.index', [
            'allBooks' => Book::select()->orderBy('updated_at', 'DESC')->get()
        ]);
    }

    /**
     * Show add book form
     *
     * @return void
     */
    public function add()
    {
        return view('books.add');
    }

    /**
     * Store new book
     *
     * @return void
     */
    public function store(BookStoreRequest $request)
    {
        return redirect(
            route('show', [
                'book' => Book::create($request->validated())
            ])
        );
    }

    /**
     * Show single book details.
     *
     * @param Book $book
     * @return void
     */
    public function show(Book $book)
    {
        return view('books.show', ['book' => $book]);
    }

    /**
     * Show edit book form
     *
     * @param Book $book
     * @return void
     */
    public function edit(Book $book)
    {
        return view('books.edit', ['book' => $book]);
    }

    /**
     * Update book
     *
     * @param Book $book
     * @return void
     */
    public function update(UpdateBookStoreRequest $request, Book $book)
    {
        if ($book->update($request->validated())) {
            $request->session()->flash('status', [
                'success' => true,
                'message' => 'Zmiany zostały zapisane.'
            ]);
        } else {
            $request->session()->flash('status', [
                'success' => false,
                'message' => 'Wystąpił błąd podczas zapisu.'
            ]);
        }

        return redirect(
            route('show', [
                'book' => $book
            ])
        );
    }

    /**
     * Delete book
     *
     * @param Book $book
     * @return void
     */
    public function delete(Request $request, Book $book)
    {
        if ($book->delete()) {
            $request->session()->flash('status', [
                'success' => true,
                'message' => 'Książka została usunięta.'
            ]);
        } else {
            $request->session()->flash('status', [
                'success' => false,
                'message' => 'Wystąpił błąd podczas usuwania.'
            ]);
        }

        return redirect(
            route('index')
        );
    }

    /**
     * API Get books
     *
     * @return void
     */
    public function get_books(Request $request)
    {
        return response()->json(Book::select()->orderBy('updated_at', 'DESC')->get());
    }
}
