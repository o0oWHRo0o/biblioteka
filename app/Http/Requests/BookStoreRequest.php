<?php

namespace App\Http\Requests;

use App\Models\Book;
use Illuminate\Foundation\Http\FormRequest;

class BookStoreRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required|string|max:200',
            'author'        => 'required|string|min:3|max:255',
            'release_date'  => 'required|date|date_format:Y-m-d'
        ];
    }
}
