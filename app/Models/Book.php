<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Book model.
 * 
 * @property string $title
 * @property string $slug
 * @property string $author
 * @property date $release_date
 * 
 */
class Book extends Model
{
    use HasFactory;

    /**
     * Fillable attributes
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'author', 'release_date'
    ];

    /**
     * Model boot function
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
    }

    /**
     * Model route key name
     *
     * @return void
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
