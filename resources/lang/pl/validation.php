<?php

return [

    'required' => 'To pole jest wymagane.',
    'min' => [
        'numeric' => 'To pole musi mieć wartość minimalną :min.',
        'file' => 'To pole musi mieć minimum :min kilobajtów.',
        'string' => 'To pole musi mieć minimum :min znaków.',
        'array' => 'To pole musi mieć minimum :min elementów.',
    ],    
    'max' => [
        'numeric' => 'To pole może mieć wartość maksymalną :max.',
        'file' => 'To pole musi może maksimum :max kilobajtów.',
        'string' => 'To pole musi może maksimum :max znaków.',
        'array' => 'To pole musi może maksimum :max elementów.',
    ],
    'date' => 'To pole nie zawiera poprawnej daty.',
    'date_format' => 'To pole musi być w formacie :format.',
    'string' => 'To pole musi zawierać tekst.',


];
