@extends('layouts.app')

@section('title', 'Biblioteka')

@section('content')
    <div class="container">
        <div class="row-py-15">        
            <div class="col-sm-12 col-lg-8 offset-lg-2 py-3">                
                @include('layouts.nav')
                @include('layouts.flash')
                @include('layouts.list')
            </div>
        </div>
    </div>
@endsection
