@extends('layouts.app')

@section('title', 'Biblioteka')

@section('content')
    <div class="container">
        <div class="row-py-15">
            <div class="col-sm-12 col-lg-8 offset-lg-2 py-3">
                <nav class="d-flex w-100 justify-content-between mb-3">
                    <ul class="nav nav-pills">
                        <li class="nav-item px-2">
                            <a href="{{ route('index') }}" class="btn btn-primary">Lista książek</a>
                        </li>
                    </ul>
                </nav>

                <form action="{{ route('store') }}" method="POST">
                    <div class="mb-3">
                        <label for="title" class="form-label">Tytuł książki</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" id="title"
                            value="{{ old('title') }}">
                        @error('title')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="author" class="form-label">Autor książki</label>
                        <input type="text" class="form-control @error('author') is-invalid @enderror" name="author"
                            id="author" value="{{ old('author') }}">
                        @error('author')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="release_date" class="form-label">Data wydania</label>
                        <input type="text" class="form-control datepicker @error('release_date') is-invalid @enderror"
                            name="release_date" id="release_date" value="{{ old('release_date') }}">
                        @error('release_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-success">Zapisz</button>
                    @csrf
                    @method('POST')
                </form>
            </div>
        </div>
    </div>
@endsection
