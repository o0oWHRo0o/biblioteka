@extends('layouts.app')

@section('title', 'Biblioteka')

@section('content')
    <div class="container">
        <div class="row-py-15">
            <div class="col-sm-12 col-lg-8 offset-lg-2">
                @include('layouts.nav')
                @include('layouts.flash')

                <div class="card text-dark bg-light">
                    <div class="card-header h2">{{ $book->title }}</div>
                    <div class="card-body">
                        <h5 class="card-title">{{ $book->author }}</h5>
                        <p class="card-text">Data wydania: {{ $book->release_date }}</p>

                        <div class="d-flex w-100 justify-content-between mb-3">
                            <a href="{{ route('edit', ['book' => $book]) }}" class="btn btn-primary">Modyfikiuj</a>

                            <form action="{{ route('delete', ['book' => $book]) }}" method="POST" novalidate>
                                <button type="submit" class="btn btn-danger">Usuń</button>
                                @csrf
                                @method('DELETE')
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
