<nav class="d-flex w-100 justify-content-between mb-3">
    <ul class="nav nav-pills">
        <li class="nav-item px-2">
            <a href="{{ route('index') }}" class="btn btn-primary">Lista książek</a>
        </li>
    </ul>
    <ul class="nav nav-pills">
        <li class="nav-item px-2">
            <a href="{{ route('add') }}" class="btn btn-success">Dodaj nową książkę</a>
        </li>
    </ul>
</nav>
