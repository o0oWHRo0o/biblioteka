@if (session()->has('status'))
<div class="col-md-12">
    <div class="alert @if (session('status')['success']) alert-success @else alert-danger @endif" role="alert">
        {{ session('status')['message'] }}
    </div>
</div>
@endif