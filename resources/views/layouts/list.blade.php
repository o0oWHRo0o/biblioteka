<div class="tab-content">
    <div class="tab-pane fade show active" id="books_all" role="tabpanel">

        @forelse ($allBooks as $book)
            <div class="card text-dark bg-light mb-3">
                <div class="card-header h2">{{ $book->title }}</div>
                <div class="card-body">
                    <div class="d-flex w-100 justify-content-between mb-3">
                        <div class="col-md-10">
                            <h5 class="card-title">{{ $book->author }}</h5>
                            <p class="card-text">Data wydania: {{ $book->release_date }}</p>
                        </div>
                        <div class="col-md-2">
                            <div class="btn-group" role="group">
                                <div class="btn-group" role="group">
                                    <button type="button" class="btn btn-primary dropdown-toggle"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                        Akcje
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('show', ['book' => $book]) }}"
                                                class="dropdown-item">Szczegóły</a></li>
                                        <li><a href="{{ route('edit', ['book' => $book]) }}"
                                                class="dropdown-item">Modyfikiuj</a></li>
                                        <li>
                                            <form action="{{ route('delete', ['book' => $book]) }}" method="POST"
                                                novalidate>
                                                <button type="submit" class="dropdown-item">Usuń</button>
                                                @csrf
                                                @method('DELETE')
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <p>Tutaj pojawi się lista książek</p>
        @endforelse

    </div>
</div>
